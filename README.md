# Dependencies

- curl
- dos2unix
- jq
- csvkit
- sqlite3
- datasette (optional)

# How to run

## Without Docker

```bash
./scripts/generate.sh
```

The dataset can be published using Datasette:

```bash
./scripts/publish.sh
```

## Using Docker

```bash
mkdir output
docker run --rm \
    -v $PWD/output:/root/output \
    -p 8001:8001 \
    registry.gitlab.com/nina-data/alien-species-artsdatabanken/main
```
