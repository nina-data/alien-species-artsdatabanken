-- created using csvsql (csvkit) and then improved
CREATE TABLE species (
        "assessmentId" VARCHAR PRIMARY KEY,
        "scientificNameId" INTEGER NOT NULL,
        "scientificName" VARCHAR NOT NULL,
        "vernacularName" VARCHAR,
        "expertGroup" VARCHAR NOT NULL,
        "speciesGroup" VARCHAR NOT NULL,
        "riskCategory" VARCHAR NOT NULL,
        "evaluationCategory" VARCHAR NOT NULL,
        criteria JSON,
        counties JSON,
        diff2012 INTEGER NOT NULL,
        environments JSON,
        habitats JSON,
        vectors JSON,
        "climateEffects" JSON,
        parasite BOOLEAN NOT NULL,
        reproduction VARCHAR,
        "geoVariation" BOOLEAN NOT NULL
);
