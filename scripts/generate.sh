#!/bin/bash

set -O inherit_errexit &>/dev/null # Bash >= 4.4
set -eET -o pipefail

: "${db:=output/alien_species.sqlite}"
mkdir -p $(dirname "$db")

# custom schema because of https://github.com/wireservice/csvkit/issues/1070#issuecomment-859771071
curl -s 'https://artsdatabanken.no/fab2018/?mode=headless' |
    sed 's/\r//' `#dos2unix alternative` |
    sed -nr 's/^ *data: (.*),$/\1/p' |
    jq -r '.
        | map(.criteria |= (. | split(",") | tostring))
        | map(.counties |= (. | split(",") | tostring))
        | map(.environments |= (. | split(",") | tostring))
        | map(.climateEffects |= (. | split(",") | tostring))
        | map(.habitats |= (. | split(",") | tostring))
        | map(.vectors |= (. | ltrimstr(",") | split(",") | tostring))
    ' |
    in2csv -f json |
    csvsql --db "sqlite:///$db" \
        --no-create --before-insert "$(<sql/schema.sql)" \
        --insert --tables 'species'

#sqlite3 "$db" "VACUUM"
