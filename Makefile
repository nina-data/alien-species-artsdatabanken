.PHONY: clean all
.DELETE_ON_ERROR:
OUTPUT=output

all: $(OUTPUT)/alien_species.sqlite

$(OUTPUT)/alien_species.sqlite : scripts/generate.sh sql/schema.sql 
	mkdir -p $(OUTPUT)
	db=$@ $<

clean :
	rm -rf $(OUTPUT)
